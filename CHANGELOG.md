# Change log

This document summerizes the cumulative changes in genome annoations
since the initial annotation retieved from the EBI.

## v1.2 (2021-05-18)

### Edited

* Delete all standard_name qualifiers.

## v1.1 (2021-05-17)

### Edited

* Build feature hierarchy.

### Fixed

* Repeat_region on both strand: LAQU0_S11e03312t.
* Bad intron coordinates: LAQU0_S05e04764g.
* mRNA with bad coordinates: LAQU0_S10e01398g.
* mRNA and gene with bad coordinates: LAQU0_S13e03202g.

## v1.0 (2021-05-17)

### Added

* The 51 scaffolds of Lachancea quebecensis CBS 14088 (source EBI, [GCA_002900925.1](https://www.ebi.ac.uk/ena/browser/view/GCA_002900925.1)).
