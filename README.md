## *Lachancea nothofagi* CBS 14088

### Source

* **Data source**: [ENA](https://www.ebi.ac.uk/ena/browser/home)
* **BioProject**: [PRJEB8847](https://www.ebi.ac.uk/ena/browser/view/PRJEB8847)
* **Assembly accession**: [GCA_002900925.1](https://www.ebi.ac.uk/ena/browser/view/GCA_002900925.1)
* **Original submitter**: 

### Assembly overview

* **Assembly level**: Scaffold
* **Assembly name**: ASM290092v1
* **Assembly length**: 10,229,370
* **#Scaffolds**: 51
* **Mitochondiral**: No
* **N50 (L50)**: 533,706 (6)

### Annotation overview

* **Original annotator**: 
* **CDS count**: 5146
* **Pseudogene count**: 72
* **tRNA count**: 231
* **rRNA count**: 7
* **Mobile element count**: 11
